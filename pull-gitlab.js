/** @param {NS} ns **/
export async function main(ns) {
  const script = 'update-scripts.js';
  ns.tprintf('Pulling ' + script + ' off GitLab');
  await ns.wget('https://gitlab.com/Zythaar/bitburner/-/raw/main/' + script, script, 'home');
}
